export class Shortcuts {
  previousSlide = ['Up', 'PgUp'];
  nextSlide = ['Down', 'PgDown'];
  previousItem = ['Left'];
  nextItem = ['Right'];
  showDisplay = ['Space'];
  themeDisplay = ['t'];
  blankDisplay = ['.'];
  desktopDisplay = ['d'];
}
