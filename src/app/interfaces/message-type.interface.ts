export interface MessageType {
  plugin: string;
  key: string;
  value: any;
}
