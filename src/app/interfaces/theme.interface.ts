export interface Theme {
  name: string;
  selected: boolean;
  thumbnail: object;
}
